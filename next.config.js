/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    loader: "custom",
    loaderFile: "./app/utils/image-loader.ts",
  },
};

module.exports = nextConfig;
