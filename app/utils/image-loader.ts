export default function cloudinaryLoader({ src }: { src: any }) {
    // https://res.cloudinary.com/dhq9acwqr/image/upload/v1701244645/e2ptginv5aov9snumwlt.png
    //https://res.cloudinary.com/dhq9acwqr/image/upload/v1664624046/Place_Your_Image_Here_Double_Click_to_Edit_g9ye1o.png
    return `${src}`
}