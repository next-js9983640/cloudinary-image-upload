import { NextResponse } from "next/server"
import { initializeApp } from 'firebase/app';
import { getStorage, ref, uploadString, getDownloadURL } from 'firebase/storage';

const firebaseConfig = {
    storageBucket: "gs://next-dashboard-3cfe2.appspot.com",
    apiKey: "AIzaSyAuPCxTOWbYvjPv8p9qCdRzGF6ulvVa7n8",
    authDomain: "next-dashboard-3cfe2.firebaseapp.com",
    projectId: "next-dashboard-3cfe2",
    messagingSenderId: "1062132439322",
    appId: "1:1062132439322:web:4500cf18539e21705434a3",
    measurementId: "G-669JFH4X6M"
}

const metadata = {
    contentType: 'image/jpeg',
};
export async function POST(request: Request) {

    const formData = await request.formData()
    const image: any = formData.get('image')

    if (!image) {
        return NextResponse.json({ error: 'No image provided' }, { status: 400 })
    }

    const bytes = await image.arrayBuffer()
    const buffer = Buffer.from(bytes)
    const fileBase64 = buffer.toString('base64')

    const app = initializeApp(firebaseConfig);

    const storage = getStorage(app);

    const storageRef = ref(storage, image.name)

    await uploadString(storageRef, fileBase64, 'base64', metadata)

    const firebaseImageUrl = await getDownloadURL(storageRef)

    return NextResponse.json({
        message: "Image uploaded successfully",
        url: firebaseImageUrl
    })
}