import { NextResponse } from 'next/server'
import { config } from '@/app/utils/config'
import { v2 as cloudinary } from 'cloudinary';




cloudinary.config({
    cloud_name: 'dhq9acwqr',
    api_key: '568331559254542',
    api_secret: 'NRqEk-1VhUz7Sdt-VYs5nZu1Tt4'
});

export async function POST(request: Request) {



    const formData = await request.formData()
    const image: any = formData.get('image')

    if (!image) {
        return NextResponse.json({ error: 'No image provided' }, { status: 400 })
    }
    const bytes = await image.arrayBuffer()
    const buffer = Buffer.from(bytes)
    const fileBase64 = buffer.toString('base64')


    const response: any = await new Promise((resolve, reject) => {
        cloudinary.uploader.upload_stream({ folder: 'next-dashboard' }, (err, result) => {
            if (err) reject(err)

            resolve(result)
        }).end(buffer)
    })

    return NextResponse.json({
        message: "Image uploaded successfully",
        url: response.secure_url
    })

}