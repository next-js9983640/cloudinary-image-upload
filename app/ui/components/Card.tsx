'use client'
import {useState} from "react";

export function Card({api, title}: {api: string, title: string}){
    const [file, setFile] = useState<any>(null)
    const [image, setImage] = useState<any>(null)
    return(
        <div className='relative flex flex-col justify-center h-screen items-center'>
            {image && (
                <h1 className='absolute glass py-2 px-3 rounded-2xl'>{title}</h1>
            )}
            <div className={`w-full ${image ? 'h-screen' : ''}`}
                 style={{
                     backgroundImage: `url(${image})`,
                     backgroundSize: 'cover',
                     backgroundPosition: 'center',
                     backgroundRepeat: 'no-repeat'
                 }}
            >
                {!image && (
                    <form onSubmit={async (e) => {
                        e.preventDefault()
                        const formData = new FormData()
                        formData.append('image', file)
                        const response = await fetch(`${api}`, {
                            method: 'POST',
                            body: formData
                        })
                        const data = await response.json()
                        setImage(data.url)
                    }}>
                        <div className="flex flex-col relative items-center justify-center md:w-[100%] h-[60vh]">
                            <label
                                htmlFor="dropzone-file"
                                className="flex flex-col items-center justify-center w-full md:w-[50%] h-full border-2 border-gray-300 border-dashed rounded-lg cursor-pointer"
                                style={{
                                    backgroundImage: `url(${file ? URL.createObjectURL(file) : 'https://images.unsplash.com/photo-1622837137196-4b9b0b0b5b0f?ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwzNnx8fGVufDB8fHx8'})`,
                                    objectFit: 'contain',
                                    backgroundSize: 'cover',
                                    backgroundPosition: 'right',
                                    backgroundRepeat: 'no-repeat'
                                }}
                            >
                                {!file && (<div className="flex flex-col items-center justify-center pt-5 pb-6">
                                    <p className="mb-2 text-sm text-gray-500 dark:text-gray-400">
                                        <span className="font-semibold">Click to upload</span> or drag and drop
                                    </p>
                                    <p className="text-xs text-gray-500 dark:text-gray-400">
                                        SVG, PNG, JPG or GIF (MAX. 800x400px)
                                    </p>
                                </div>)}
                                <input onChange={(e) => {
                                    const selectedFile = e.target.files?.[0]
                                    setFile(selectedFile)
                                }} id="dropzone-file" type="file" className="hidden" />
                            </label>
                            {file && (
                                <button className='my-5 flex justify-center items-center bg-slate-600 text-white px-2 py-3 rounded-2xl' type='submit'>send file</button>
                            )}
                        </div>

                    </form>
                )}
            </div>
        </div>
    )
}