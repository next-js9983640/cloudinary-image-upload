import Link from "next/link";

export function NavBar(){
    return(
        <nav className=''>
            <ul className='flex fixed glass py-2 px-3 top-0 left-0 right-0 z-20 justify-between w-full items-center'>
                <li className='py-2 px-3 bg-indigo-600 text-white'>
                    <Link href='/'>Home</Link>
                </li>
                <li className='py-2 px-3 bg-indigo-600 text-white'>
                    <Link href='/cloudinary'>Cloudinary</Link>
                </li>
                <li className='py-2 px-3 bg-indigo-600 text-white'>
                    <Link href='/firebase'>Firebase</Link>
                </li>
            </ul>
        </nav>
    )
}