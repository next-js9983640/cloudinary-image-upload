"use client"



export default function Home() {

  return (
    <main className="flex flex-col justify-center items-center">
      <h1 className="text-4xl p-4 font-bold text-indigo-600">Image Uploader <code>Firebase</code> and <code>Cloudinary</code></h1>
        <div className='flex md:flex-row flex-col gap-7 p-4'>
            <div className='text-center text-indigo-600 text-2xl'>
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    x="0px"
                    y="0px"
                    width={200}
                    height={200}
                    viewBox="0 0 48 48"
                >
                    <path
                        fill="#ff8f00"
                        d="M8,37L23.234,8.436c0.321-0.602,1.189-0.591,1.494,0.02L30,19L8,37z"
                    />
                    <path
                        fill="#ffa000"
                        d="M8,36.992l5.546-34.199c0.145-0.895,1.347-1.089,1.767-0.285L26,22.992L8,36.992z"
                    />
                    <path
                        fill="#ff6f00"
                        d="M8.008 36.986L8.208 36.829 25.737 22.488 20.793 13.012z"
                    />
                    <path
                        fill="#ffc400"
                        d="M8,37l26.666-25.713c0.559-0.539,1.492-0.221,1.606,0.547L40,37l-15,8.743 c-0.609,0.342-1.352,0.342-1.961,0L8,37z"
                    />
                </svg>
                <h1>Firebase</h1>
            </div>

            <div className='text-center text-indigo-600 text-2xl'>
                <svg
                    id="cld-main-logo"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 100 100"
                    overflow={"visible"}
                    width={200}
                    height={200}
                >
                    <g id="Layer_2" data-name="Layer 2">
                        <g id="Layer_1-2" data-name="Layer 1">
                            <path
                                width={100}
                                height={100}
                                className="cls-1"
                                fill="#4f46e5"
                                d="M97.91,28.11A40.38,40.38,0,0,0,59.73,0,39.62,39.62,0,0,0,24.6,20.87a29.88,29.88,0,0,0-7.21,56.56l.75.34h.05v-8.5a22.29,22.29,0,0,1,9.29-41.16l2.1-.22L30.5,26A32.15,32.15,0,0,1,59.73,7.57a32.7,32.7,0,0,1,31.55,25L92,35.43l3,0a18.53,18.53,0,0,1,18.15,18.46c0,7.05-4.07,12.82-11,15.74v8.06l.5-.16c11.14-3.65,18.06-12.71,18.06-23.64A26.19,26.19,0,0,0,97.91,28.11Z"
                            />
                            <path
                                className="cls-1"
                                fill="#4f46e5"
                                d="M45.07,76.79l1.66,1.66a.33.33,0,0,1-.23.56H33.4a6,6,0,0,1-6-6V47.57a.33.33,0,0,0-.33-.33H24.27a.33.33,0,0,1-.24-.56L35.15,35.56a.33.33,0,0,1,.47,0L46.73,46.68a.33.33,0,0,1-.23.56H43.66a.34.34,0,0,0-.34.33v25A6,6,0,0,0,45.07,76.79Z"
                            />
                            <path
                                className="cls-1"
                                fill="#4f46e5"
                                d="M69.64,76.79l1.67,1.66a.33.33,0,0,1-.24.56H58a6,6,0,0,1-6-6V54a.34.34,0,0,0-.33-.34H48.84a.33.33,0,0,1-.23-.56L59.72,42a.33.33,0,0,1,.47,0L71.31,53.08a.33.33,0,0,1-.24.56H68.23a.34.34,0,0,0-.33.34V72.57A6,6,0,0,0,69.64,76.79Z"
                            />
                            <path
                                className="cls-1"
                                fill="#4f46e5"
                                d="M94.22,76.79l1.66,1.66a.33.33,0,0,1-.23.56H82.54a6,6,0,0,1-6-6V60.38a.33.33,0,0,0-.33-.33H73.41a.33.33,0,0,1-.23-.57L84.3,48.37a.32.32,0,0,1,.46,0L95.88,59.48a.33.33,0,0,1-.23.57H92.8a.33.33,0,0,0-.33.33V72.57A6,6,0,0,0,94.22,76.79Z"
                            />
                        </g>
                    </g>
                </svg>
                <h1>Cloudinary</h1>
            </div>
        </div>
    </main>
  )
}
