import {Card} from "@/app/ui/components/Card";

export default function Page(){

    return (
        <Card api={'/api/uploadFirebase'} title={'Firebase image uploader'} />
    )
}